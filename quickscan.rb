#!/usr/bin/env ruby
#
require 'http'
require 'timeout'
require 'rainbow/ext/string'
require 'netaddr'
require 'thread/pool'
require 'thread'
require 'optparse'
require 'pp'

class Parser
    def self.parse(options)
        args = {}
        args[:port] = 80
        args[:timeout] = 1.0
        args[:thread] = 8
        args[:verbose] = 0

        opt_parser = OptionParser.new do |opts|
            opts.banner = "Usage: #{$0} [options]"

            opts.on("-iIP", "--i=IP", "Target IP to scan") do |n|
                args[:victim] = n
            end

            opts.on("-ffile", "--file=file", "Target in the file to scan") do |n|
                args[:file] = n
            end

            opts.on("-pPort", "--port=port", "Target port to scan") do |n|
                args[:port] = n.to_i
            end

            opts.on("-stimeout", "--second=timeout", "Timeout for each reqeust (default: 1.0 second)") do |n|
                args[:timeout] = n.to_f
            end

            opts.on("-tthread", "--thread=thread", "Thread number (default: 8)") do |n|
                args[:thread] = n.to_i
            end

            opts.on("-vverbose", "--verbose=verbose", "Verbose (log level)") do |n|
                args[:verbose] = n.to_i
            end

            opts.on("-h", "--help", "Prints this help") do
                puts opts
                exit
            end
        end

        opt_parser.parse!(options)
        return args
    end
end

@mutex = Mutex.new

def query(ip, options)
    host = "http://#{ip}:#{options[:port]}"
    begin
        Timeout.timeout(options[:timeout]) do
            response = HTTP["Accept" => "*/*", "User-Agent" => "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)"].get(host)
            server = response["Server"]
            title = response.to_s[/<title>(.*)<\/title>/, 1]
            server = "" if server.nil?
            title = "" if title.nil?
            @mutex.synchronize do
                puts "%-32s %5s %-10s %s" % [host.color(:green), response.code.to_s.color(:yellow), server.color(:cyan), title.color(:yellow)]
            end
        end
    rescue Timeout::Error => e
        if options[:verbose] != 0
            @mutex.synchronize do
                puts "%-32s %-25s" % [host.color(:red), "Timeout".color(:yellow)]
            end
        end
    rescue => e
        if options[:verbose] != 0
            @mutex.synchronize do
                pp e
            end
        end
    end
end

if $0 == __FILE__
    options = Parser.parse ARGV
    pool = Thread.pool options[:thread]
    pp options
    if options.has_key? :victim
        NetAddr::CIDR.create(options[:victim]).enumerate.each do |ip|
            pool.process ip, options do |a_ip, a_options|
                query a_ip, a_options
            end
        end
    elsif options.has_key? :file
        File.open(options[:file]).each_line do |line|
            line.scan(/\b\d+\.\d+\.\d+\.\d+\b/) do |ip|
                pool.process ip, options do |a_ip, a_options|
                    query a_ip, a_options
                end
            end
        end
    else
        Parser.parse %w{--help}
    end
    pool.shutdown
end

